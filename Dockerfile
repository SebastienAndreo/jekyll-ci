FROM ruby:2.7

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get install dialog


RUN apt-get update \
    && apt-get install -y  gnupg \
    && apt-get install -y  wget python-pip

RUN pip install --upgrade python-gitlab

# Install OpenJDK-11
RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y ant && \
    apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME


# Test java has been installed
RUN java -version

# install graphviz
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        graphviz \
    && rm -rf /var/lib/apt/lists/*


# install plantuml
ENV PLANTUML_VERSION=1.2020.26

RUN mkdir /opt/plantuml \
    && wget "http://downloads.sourceforge.net/project/plantuml/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar" -O /opt/plantuml/plantuml.jar \
    && echo '#!/bin/bash\n java -Dplantuml.include.path="/home/DSL/" -jar /opt/plantuml/plantuml.jar -tsvg "$1" "$2"' > /opt/plantuml/plantuml.sh \
    && chmod +x /opt/plantuml/plantuml.sh \
    && ln -s /opt/plantuml/plantuml.sh /usr/bin/plantuml

# install PLant Uml Dialects
ADD DSL /home/DSL

# Install AWS DSL
ENV AWSPUML_VERSION=5.0
RUN mkdir /home/awstemp \
    && wget "https://github.com/awslabs/aws-icons-for-plantuml/archive/v${AWSPUML_VERSION}.zip" -O /home/awstemp/v${AWSPUML_VERSION}.zip\
    && unzip /home/awstemp/v${AWSPUML_VERSION}.zip -d /home/awstemp/\
    && cp -vr /home/awstemp/aws-icons-for-plantuml-${AWSPUML_VERSION}/dist /home/DSL/AWS \
    && rm -r  /home/awstemp/

RUN chmod -R 666 /home/DSL/AWS/ \
    && ls -l /home/DSL/AWS/


# install Azure DSL
ENV AZUREPUML_VERSION=2.1
RUN mkdir /home/azuretemp \
    && wget "https://github.com/RicardoNiepel/Azure-PlantUML/archive/v${AZUREPUML_VERSION}.zip" -O /home/azuretemp/v${AZUREPUML_VERSION}.zip \
    && unzip /home/azuretemp/v${AZUREPUML_VERSION}.zip -d /home/azuretemp/ \
    && cp -vr /home/azuretemp/Azure-PlantUML-${AZUREPUML_VERSION}/dist /home/DSL/Azure \
    && rm -r  /home/azuretemp/

RUN chmod -R 666 /home/DSL/Azure/ \
    && ls -l /home/DSL/Azure/

RUN gem install bundler --source https://rubygems.org \
    & gem install rake --source https://rubygems.org \
    & gem install html-proofer --source https://rubygems.org



# test plantuml and inderectly graphviz installation
COPY test.uml /home/test.UML
RUN plantuml -tsvg /home/test.UML
